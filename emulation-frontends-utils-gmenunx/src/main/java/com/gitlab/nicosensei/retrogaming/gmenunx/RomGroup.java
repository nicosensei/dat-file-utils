package com.gitlab.nicosensei.retrogaming.gmenunx;

import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGamelist;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class RomGroup {

    private Map<String, SparseGamelist> roms = new LinkedHashMap<>();
    private Set<String> thumbnails = new LinkedHashSet<>();

    public Set<String> getThumbnails() {
        return thumbnails;
    }

    public RomGroup addThumbnails(final Set<String>  thumbnails) {
        thumbnails.addAll(thumbnails);
        return this;
    }

    public Map<String, SparseGamelist> getRoms() {
        return roms;
    }

    public RomGroup addRoms(final String folder, final SparseGamelist gamelist) {
        roms.put(folder, gamelist);
        return this;
    }
}
