package com.gitlab.nicosensei.retrogaming.gmenunx.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class RomFolderSettings {

    private static final String DEFAULT_THUMBAILS_FOLDER = "images";

    private boolean useGamelistName = true;
    private Set<String> groups = new HashSet<>();
    private String previews = DEFAULT_THUMBAILS_FOLDER;
    private Set<String> thumbnails = new HashSet<>();

    public String getPreviews() {
        return previews;
    }

    public RomFolderSettings setPreviews(String previews) {
        this.previews = previews;
        return this;
    }

    public boolean isUseGamelistName() {
        return useGamelistName;
    }

    public RomFolderSettings setUseGamelistName(boolean useGamelistName) {
        this.useGamelistName = useGamelistName;
        return this;
    }

    public Set<String> getThumbnails() {
        return thumbnails;
    }

    public Set<String> getGroups() {
        return groups;
    }

    public RomFolderSettings setGroups(Set<String> groups) {
        this.groups = groups;
        return this;
    }

    public RomFolderSettings setThumbnails(String ... thumbnails) {
        this.thumbnails = new LinkedHashSet<>(Arrays.asList(thumbnails));
        return this;
    }
}
