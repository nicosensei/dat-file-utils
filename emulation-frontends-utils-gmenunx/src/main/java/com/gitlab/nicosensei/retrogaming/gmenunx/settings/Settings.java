package com.gitlab.nicosensei.retrogaming.gmenunx.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gitlab.nicosensei.retrogaming.gmenunx.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public final class Settings {

    private static final String DEFAULT_ROM_FOLDER = "roms";
    private static final String DEFAULT_TGT_MEDIA_FOLDER = "skraper";
    private static final String DEFAULT_SRC_MEDIA_FOLDER = "media";
    private static final String DEFAULT_GAMELIST = "gamelist.xml";
    private static final String DEFAULT_ALIASES_PREFIX = "aliases_";
    private static final String DEFAULT_PREVIEWS_FOLDER = ".previews";

    private String scriptOutputFolder;
    private String sdcardMountPoint;
    private String targetMediaFolder = DEFAULT_TGT_MEDIA_FOLDER;
    private String sourceMediaFolder = DEFAULT_SRC_MEDIA_FOLDER;
    private String romRootFolder = DEFAULT_ROM_FOLDER;
    private String gamelistName = DEFAULT_GAMELIST;
    private String aliasFileNamePrefix = DEFAULT_ALIASES_PREFIX;
    private String previewsFolder = DEFAULT_PREVIEWS_FOLDER;

    private Target target = Target.retrofw;

    private Map<String, RomFolderSettings> roms = new HashMap<>();

    public String getScriptOutputFolder() {
        return scriptOutputFolder;
    }

    public Settings setScriptOutputFolder(String scriptOutputFolder) {
        this.scriptOutputFolder = scriptOutputFolder;
        return this;
    }

    public String getSdcardMountPoint() {
        return sdcardMountPoint;
    }

    public Settings setSdcardMountPoint(String sdcardMountPoint) {
        this.sdcardMountPoint = sdcardMountPoint;
        return this;
    }

    public String getTargetMediaFolder() {
        return targetMediaFolder;
    }

    public Settings setTargetMediaFolder(String targetMediaFolder) {
        this.targetMediaFolder = targetMediaFolder;
        return this;
    }

    public String getSourceMediaFolder() {
        return sourceMediaFolder;
    }

    public Settings setSourceMediaFolder(String sourceMediaFolder) {
        this.sourceMediaFolder = sourceMediaFolder;
        return this;
    }

    public String getRomRootFolder() {
        return romRootFolder;
    }

    public Settings setRomRootFolder(String romRootFolder) {
        this.romRootFolder = romRootFolder;
        return this;
    }

    public String getGamelistName() {
        return gamelistName;
    }

    public Settings setGamelistName(String gamelistName) {
        this.gamelistName = gamelistName;
        return this;
    }

    public String getAliasFileNamePrefix() {
        return aliasFileNamePrefix;
    }

    public Settings setAliasFileNamePrefix(String aliasFileNamePrefix) {
        this.aliasFileNamePrefix = aliasFileNamePrefix;
        return this;
    }

    public String getPreviewsFolder() {
        return previewsFolder;
    }

    public Settings setPreviewsFolder(String previewsFolder) {
        this.previewsFolder = previewsFolder;
        return this;
    }

    public Target getTarget() {
        return target;
    }

    public Settings setTarget(Target target) {
        this.target = target;
        return this;
    }

    public Map<String, RomFolderSettings> getRoms() {
        return roms;
    }

    public Settings setRoms(Map<String, RomFolderSettings> roms) {
        this.roms = roms;
        return this;
    }
}
