package com.gitlab.nicosensei.retrogaming.gmenunx;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGamelist;
import com.gitlab.nicosensei.retrogaming.gmenunx.processor.Gamelist2Aliases;
import com.gitlab.nicosensei.retrogaming.gmenunx.processor.RG350PreviewsScript;
import com.gitlab.nicosensei.retrogaming.gmenunx.processor.ThumbnailsScript;
import com.gitlab.nicosensei.retrogaming.gmenunx.settings.RomFolderSettings;
import com.gitlab.nicosensei.retrogaming.gmenunx.settings.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class EmulationStation2GMenuNX {

    private static final Logger LOG = LoggerFactory.getLogger(EmulationStation2GMenuNX.class);

    public static final void main(final String... args) throws IOException {
        if (args.length != 1) {
            LOG.error(
                    "Usage: java {} <{}>",
                    EmulationStation2GMenuNX.class.getCanonicalName(),
                    "path to settings JSON file");
            System.exit(-1);
        }

        new EmulationStation2GMenuNX().run(new ObjectMapper().readValue(new File(args[0]), Settings.class));
    }

    public final void run(final Settings settings) throws IOException {
        final Target target = settings.getTarget();
        final Map<String, RomFolderSettings> romFolders = settings.getRoms();
        final Map<String, SparseGamelist> gamelists = new HashMap<>(romFolders.size());
        final Map<String, RomGroup> groups = new HashMap<>(romFolders.size());

        LOG.info("Target GMenuxNX version is '{}'", target);

        // Build groups and load gamelists
        for (final String folder : romFolders.keySet()) {
            LOG.info("Processing rom folder '{}'", folder);

            final RomFolderSettings romFolderSettings = romFolders.get(folder);

            for (final String g : romFolderSettings.getGroups()) {
                RomGroup romGroup = groups.get(g);
                if (romGroup == null) {
                    romGroup = new RomGroup().addThumbnails(romFolderSettings.getThumbnails());
                    groups.put(g, romGroup);
                }

                final SparseGamelist gamelist = loadGamelist(settings, folder, romFolderSettings.isUseGamelistName());
                romGroup.addRoms(folder, gamelist);
                gamelists.put(folder, gamelist);
            }
        }

        for (final String groupName : groups.keySet()) {
            final RomGroup group = groups.get(groupName);
            final List<SparseGamelist> groupGamelists = group.getRoms().values().stream()
                    .collect(Collectors.toList());
            final SparseGamelist[] glArray = groupGamelists.toArray(new SparseGamelist[groupGamelists.size()]);
            new Gamelist2Aliases(settings, groupName, group).process(glArray);
            if (Target.retrofw.equals(target)) {
                new ThumbnailsScript(settings, groupName, group).process(glArray);
            }
        }

        if (Target.rg350.equals(target)) {
            for (final String folder : romFolders.keySet()) {
                new RG350PreviewsScript(settings, folder, romFolders.get(folder)).process(gamelists.get(folder));
            }
        }
    }

    private final SparseGamelist loadGamelist(
            final Settings settings,
            final String romFolder,
            final boolean useGamelistName) throws IOException {
        final String glPath = FileSystems.getDefault().getPath(
                settings.getSdcardMountPoint(),
                settings.getRomRootFolder(),
                romFolder,
                settings.getGamelistName()
        ).toString();

        final FileInputStream fis = new FileInputStream(glPath);
        try {
            final SparseGamelist gl = SparseGamelist.load(fis, useGamelistName);
            LOG.info("Read gamelist '{}'", glPath);
            return gl;
        } finally {
            fis.close();
        }
    }

}
