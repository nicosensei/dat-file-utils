package com.gitlab.nicosensei.retrogaming.gmenunx.processor;

import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGame;
import com.gitlab.nicosensei.retrogaming.gmenunx.RomGroup;
import com.gitlab.nicosensei.retrogaming.gmenunx.settings.Settings;
import com.gitlab.nicosensei.retrogaming.processor.Gamelist2AggregateFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public final class Gamelist2Aliases extends Gamelist2AggregateFile<Map<String, String>> {

    private static final String FMT_ALIAS_FILE_SUFFIX = "%s.txt";
    private static final String SEP = "=";

    private final Settings settings;
    private final String romGroupName;
    private final RomGroup romGroup;

    public Gamelist2Aliases(
            final Settings settings,
            final String romGroupName,
            final RomGroup romGroup) {
        this.romGroupName = romGroupName;
        this.settings = settings;
        this.romGroup = romGroup;
    }

    @Override
    protected Map<String, String> initializeResult() throws IOException {
        return new HashMap<>();
    }

    @Override
    protected void updateResult(final Map<String, String> result, final SparseGame game) throws IOException {
        final String path = game.getPath().getLeft();
        final String alias;
        result.put(path.substring(path.lastIndexOf(File.separator) + 1), game.getName());
    }

    @Override
    protected String serialize(Map<String, String> result) throws IOException {
        final TreeMap<String, String> sortedResults = new TreeMap<>(result);
        final StringBuilder sb = new StringBuilder();
        sortedResults.forEach((k, v) -> sb.append(k).append(SEP).append(v).append(System.lineSeparator()));
        return sb.toString().trim();
    }

    @Override
    protected String outputFolder() {
        return FileSystems.getDefault().getPath(
                settings.getSdcardMountPoint(),
                settings.getTargetMediaFolder()).toString();
    }

    @Override
    protected String outputFileName() {
        return String.format(
                settings.getAliasFileNamePrefix() + FMT_ALIAS_FILE_SUFFIX,
                romGroupName);
    }

}
