package com.gitlab.nicosensei.retrogaming.gmenunx.processor;

import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGame;
import com.gitlab.nicosensei.retrogaming.gmenunx.RomGroup;
import com.gitlab.nicosensei.retrogaming.gmenunx.settings.Settings;
import com.gitlab.nicosensei.retrogaming.processor.Gamelist2ShellScript;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.text.MessageFormat;

public final class ThumbnailsScript extends Gamelist2ShellScript {

    private static final String FMT_SCRIPT_FILE_NAME = "thumbnails_%s.sh";

    private static final String MKDIR_THUMBNAIL_FOLDER = "mkdir -p \"%s\"";
    private static final String MV_IMAGE = "if [ -f \"{0}.png\" ]; then mv -vf \"{0}.png\" \"{1}.png\"; fi";
    private static final String RM_FILE = "if [ -f {0} ]; then rm -vf {0}; fi";
    private static final String RM_DIR = "if [ -d {0} ]; then rm -rvf {0}; fi";

    private static final String NEWLINE = System.lineSeparator();

    private final String romGroupName;
    private final Settings settings;
    private final RomGroup romGroup;

    public ThumbnailsScript(
            final Settings settings,
            final String romGroupName,
            final RomGroup romGroup) {
        super(settings.getScriptOutputFolder(), String.format(FMT_SCRIPT_FILE_NAME, romGroupName));
        this.romGroupName = romGroupName;
        this.romGroup = romGroup;
        this.settings = settings;
    }

    @Override
    protected void writeHeader(PrintWriter pw) {
        super.writeHeader(pw);

        for (String thumbCat : romGroup.getThumbnails()) {
            pw.println(String.format(
                    MKDIR_THUMBNAIL_FOLDER,
                    FileSystems.getDefault().getPath(
                            settings.getSdcardMountPoint(),
                            settings.getTargetMediaFolder(),
                            romGroupName,
                            thumbCat).toString()));
        }
        pw.println();
    }

    @Override
    protected void writeFooter(PrintWriter pw) {
        super.writeFooter(pw);
        for (final String romFolder : romGroup.getRoms().keySet()) {
            for (final String cat : romGroup.getThumbnails()) {
                pw.println();
                pw.println(MessageFormat.format(
                        RM_FILE,
                        FileSystems.getDefault().getPath(
                                settings.getSdcardMountPoint(),
                                settings.getRomRootFolder(),
                                romFolder,
                                settings.getGamelistName()).toString()));
                pw.println(MessageFormat.format(
                        RM_DIR,
                        FileSystems.getDefault().getPath(
                                settings.getSdcardMountPoint(),
                                settings.getRomRootFolder(),
                                romFolder,
                                settings.getSourceMediaFolder()).toString()));
                pw.println();
            }
        }
    }

    @Override
    protected void updateResult(PrintWriter pw, SparseGame game) throws IOException {
        final String gamePath = game.getPath().getLeft();
        final int lastSepIdx = gamePath.lastIndexOf(File.separator);
        final Pair<String, String> splitGamePath = lastSepIdx > 0
                ? Pair.of(gamePath.substring(0, lastSepIdx), gamePath.substring(lastSepIdx + 1))
                : Pair.of("" , gamePath);

        for (final String romFolder : romGroup.getRoms().keySet()) {
            for (final String cat : romGroup.getThumbnails()) {
                pw.println(MessageFormat.format(
                        MV_IMAGE,
                        FileSystems.getDefault().getPath(
                                settings.getSdcardMountPoint(),
                                settings.getRomRootFolder(),
                                romFolder,
                                settings.getSourceMediaFolder(),
                                cat,
                                gamePath).toString(),
                        FileSystems.getDefault().getPath(
                                settings.getSdcardMountPoint(),
                                settings.getTargetMediaFolder(),
                                romGroupName,
                                cat,
                                splitGamePath.getRight()).toString()));
            }
        }

    }
}
