package com.gitlab.nicosensei.retrogaming.gmenunx.processor;

import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGame;
import com.gitlab.nicosensei.retrogaming.gmenunx.settings.RomFolderSettings;
import com.gitlab.nicosensei.retrogaming.gmenunx.settings.Settings;
import com.gitlab.nicosensei.retrogaming.processor.Gamelist2ShellScript;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;

public final class RG350PreviewsScript extends Gamelist2ShellScript {

    private static final String FMT_SCRIPT_FILE_NAME = "previews_%s.sh";

    private static final String MKDIRS = "if [ ! -d \"{0}\" ]; then mkdir -p \"{0}\"; fi";
//    private static final String MV_IMAGE = "if [ -f \"{0}.png\" ]; then mv -vf \"{0}.png\" \"{1}.png\"; fi";
    private static final String MV_IMAGE = "if [ -f \"{0}.png\" ]; then cp -vf \"{0}.png\" \"{1}.png\"; fi";

    private final Settings settings;
    private final String romFolder;
    private final RomFolderSettings romFolderSettings;

    private final Set<String> processedFolders = new HashSet<>();

    public RG350PreviewsScript(
            final Settings settings,
            final String romFolder,
            final RomFolderSettings romFolderSettings) {
        super(settings.getScriptOutputFolder(), String.format(FMT_SCRIPT_FILE_NAME, romFolder));
        this.romFolder = romFolder;
        this.romFolderSettings = romFolderSettings;
        this.settings = settings;
    }

    @Override
    protected void updateResult(PrintWriter pw, SparseGame game) throws IOException {
        final String gamePath = game.getPath().getLeft();
        final int lastSepIdx = gamePath.lastIndexOf(File.separator);
        final Pair<String, String> splitGamePath = lastSepIdx > 0
                ? Pair.of(gamePath.substring(0, lastSepIdx), gamePath.substring(lastSepIdx + 1))
                : Pair.of("", gamePath);

        final String parentPath = splitGamePath.getLeft();
        if (!processedFolders.contains(parentPath)) {
            pw.println(MessageFormat.format(MKDIRS, FileSystems.getDefault().getPath(
                    settings.getSdcardMountPoint(),
                    settings.getRomRootFolder(),
                    romFolder,
                    parentPath,
                    settings.getPreviewsFolder()).toString()));
            pw.println();
            processedFolders.add(parentPath);
        }

        final String imgName = splitGamePath.getRight();
        pw.println(MessageFormat.format(MV_IMAGE,
                FileSystems.getDefault().getPath(
                        settings.getSdcardMountPoint(),
                        settings.getRomRootFolder(),
                        romFolder,
                        settings.getSourceMediaFolder(),
                        romFolderSettings.getPreviews(),
                        parentPath,
                        imgName),
                FileSystems.getDefault().getPath(
                        settings.getSdcardMountPoint(),
                        settings.getRomRootFolder(),
                        romFolder,
                        parentPath,
                        settings.getPreviewsFolder(),
                        imgName)
                ));

    }
}
