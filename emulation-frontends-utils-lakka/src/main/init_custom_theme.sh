#!/bin/sh

LAKKA_DISK_PATH=/media/nicolas/LAKKA_DISK
CUSTOM_ASSETS_DIR=/home/nicolas/dev/git/retrogaming/lakka-customization-utils/src/main/assets/xmb

if [ -d $LAKKA_DISK_PATH/assets/xmb/custom ]; then rm -rf/home/nicolas/dev/git/retrogaming/lakka-customization-utils/src/main/init_custom_theme.sh; fi

cp -rf $LAKKA_DISK_PATH/assets/xmb/monochrome $LAKKA_DISK_PATH/assets/xmb/custom
cp -vf $LAKKA_DISK_PATH/assets/xmb/pixel/png/*.png $LAKKA_DISK_PATH/assets/xmb/custom/png
cp -vf $CUSTOM_ASSETS_DIR/custom_32/png/*.png $LAKKA_DISK_PATH/assets/xmb/custom/png