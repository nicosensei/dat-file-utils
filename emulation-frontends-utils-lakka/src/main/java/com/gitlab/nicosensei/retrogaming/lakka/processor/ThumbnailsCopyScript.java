package com.gitlab.nicosensei.retrogaming.lakka.processor;

import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGame;
import com.gitlab.nicosensei.retrogaming.lakka.RetroArchThumbnail;
import com.gitlab.nicosensei.retrogaming.lakka.settings.PlaylistSettings;
import com.gitlab.nicosensei.retrogaming.lakka.settings.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.Map;

public final class ThumbnailsCopyScript extends LakkaGamelist2ShellScript {

    private static final Logger LOG = LoggerFactory.getLogger(ThumbnailsCopyScript.class);

    private static final String MKDIR_THUMBNAIL_FOLDER = "mkdir -p \"{0}/thumbnails/{1}/{2}\"";

    private static final String PROCES_SKRAPER_IMAGE = "if [ -f \"{0}/media/{1}/{2}.png\" ]; then %s -vf \"{0}/media/{1}/{2}.png\" \"{3}/thumbnails/{4}/{5}/{6}.png\"; fi";
    private static final String CP_SKRAPER_IMAGE = String.format(PROCES_SKRAPER_IMAGE, "cp");
//    private static final String MV_SKRAPER_IMAGE = String.format(PROCES_SKRAPER_IMAGE, "mv");
    private static final String FMT_SCRIPT_NAME = "thumbnails_%s.sh";

    private static String REGEX_FILENAME_SPECIAL_CHARS = "([\\&\\*/\\:`<>\\?\\\\\\|])";
    private static String UNDERSCORE = "_";

    public ThumbnailsCopyScript(
            final Settings settings,
            final PlaylistSettings playlistSettings) {
        super(settings, playlistSettings, String.format(FMT_SCRIPT_NAME, playlistSettings.getName()));
    }

    @Override
    protected void writeHeader(PrintWriter pw) {
        super.writeHeader(pw);

        final String playlistName = playlistSettings.getName();
        final String lakkaDiskMountPoint = settings.getLakkaDiskMountPoint();

        for (final RetroArchThumbnail type : playlistSettings.getPreviews().keySet()) {
            pw.println(MessageFormat.format(MKDIR_THUMBNAIL_FOLDER, lakkaDiskMountPoint, playlistName, type.name()));
        }

        pw.println();
    }

    @Override
    protected void updateResult(PrintWriter pw, SparseGame game) throws IOException {
        final String playlistName = playlistSettings.getName();
        final String imgName = playlistSettings.isUseGamelistName() ? game.getPath().getLeft() : game.getName();

        final Map<RetroArchThumbnail, String> previews = playlistSettings.getPreviews();
        for (final RetroArchThumbnail thumb : previews.keySet()) {
            pw.println(MessageFormat.format(
                    CP_SKRAPER_IMAGE,
                            playlistSettings.getRomFolder(),
                            previews.get(thumb),
                            game.getPath().getLeft(),
                            settings.getLakkaDiskMountPoint(),
                            playlistName,
                            thumb.name(),
                            imgName.replaceAll(REGEX_FILENAME_SPECIAL_CHARS, UNDERSCORE)));
        }

        pw.println();

        LOG.info("Processed {}", SparseGame.dotConjoin(game.getPath()));
    }

}
