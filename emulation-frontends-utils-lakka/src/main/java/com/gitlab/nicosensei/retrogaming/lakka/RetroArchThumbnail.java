package com.gitlab.nicosensei.retrogaming.lakka;

public enum RetroArchThumbnail {
    Named_Boxarts,
    Named_Snaps,
    Named_Titles;
}
