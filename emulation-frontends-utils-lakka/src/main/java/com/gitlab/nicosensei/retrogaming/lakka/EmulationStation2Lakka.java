package com.gitlab.nicosensei.retrogaming.lakka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGamelist;
import com.gitlab.nicosensei.retrogaming.lakka.processor.AssetsCopyScript;
import com.gitlab.nicosensei.retrogaming.lakka.processor.Gamelist2Playlist;
import com.gitlab.nicosensei.retrogaming.lakka.processor.ThumbnailsCopyScript;
import com.gitlab.nicosensei.retrogaming.lakka.settings.PlaylistSettings;
import com.gitlab.nicosensei.retrogaming.lakka.settings.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;

public final class EmulationStation2Lakka {

    private static final Logger LOG = LoggerFactory.getLogger(EmulationStation2Lakka.class);

    public static final void main(final String... args) throws IOException {
        if (args.length != 1) {
            LOG.error(
                    "Usage: java {} <{}>",
                    EmulationStation2Lakka.class.getCanonicalName(),
                    "path to settings JSON file");
            System.exit(-1);
        }

        new EmulationStation2Lakka().run(new ObjectMapper().readValue(new File(args[0]), Settings.class));
    }

    public final void run(final Settings settings) throws IOException {
        Files.createDirectories(FileSystems.getDefault().getPath(settings.getOutputFolder()));
        for (final PlaylistSettings playlistSettings : settings.getPlaylists()) {
            LOG.info("Processing platform '{}' ({})", playlistSettings.getName(), playlistSettings.getPlatform().name());

            final SparseGamelist gamelist = readGamelist(
                    playlistSettings.getRomFolder() + File.separator + playlistSettings.getGamelist(),
                    playlistSettings.isUseGamelistName());

            new Gamelist2Playlist(settings, playlistSettings).process(gamelist);
            new AssetsCopyScript(settings, playlistSettings).process(gamelist);
            new ThumbnailsCopyScript(settings, playlistSettings).process(gamelist);
        }
    }

    private SparseGamelist readGamelist(final String path, final boolean useGamelistName) throws IOException {
        final FileInputStream fis = new FileInputStream(path);
        try {
            return SparseGamelist.load(fis, useGamelistName);
        } finally {
            fis.close();
        }
    }

}
