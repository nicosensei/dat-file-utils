package com.gitlab.nicosensei.retrogaming.lakka.playlist;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public final class Game {

    public static final String DETECT = "DETECT";
    public static final String CRC_FORMAT = "%s|crc";
    private static final Pattern CRC_REGEX = Pattern.compile("([A-Za-z0-9]{8}?)(:?\\|crc)?");


    private String path;
    private String label;
    private String corePath = DETECT;
    private String coreName = DETECT;
    private String crc32 = DETECT;
    private String dbName;

    public String getPath() {
        return path;
    }

    public Game setPath(String path) {
        this.path = path;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public Game setLabel(String label) {
        this.label = label;
        return this;
    }

    public String getCorePath() {
        return corePath;
    }

    public Game setCorePath(String corePath) {
        this.corePath = corePath;
        return this;
    }

    public String getCoreName() {
        return coreName;
    }

    public Game setCoreName(String coreName) {
        this.coreName = coreName;
        return this;
    }

    public String getCrc32() {
        return crc32;
    }

    public Game setCrc32(String crc32) {
        final Matcher m = CRC_REGEX.matcher(crc32);
        if (m.matches()) {
            this.crc32 = String.format(CRC_FORMAT, m.group(1));
        }
        return this;
    }

    public String getDbName() {
        return dbName;
    }

    public Game setDbName(String dbName) {
        this.dbName = dbName;
        return this;
    }
}
