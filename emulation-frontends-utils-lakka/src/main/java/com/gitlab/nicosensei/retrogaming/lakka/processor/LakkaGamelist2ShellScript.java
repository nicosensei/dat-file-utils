package com.gitlab.nicosensei.retrogaming.lakka.processor;

import com.gitlab.nicosensei.retrogaming.lakka.settings.PlaylistSettings;
import com.gitlab.nicosensei.retrogaming.lakka.settings.Settings;
import com.gitlab.nicosensei.retrogaming.processor.Gamelist2ShellScript;

public abstract class LakkaGamelist2ShellScript extends Gamelist2ShellScript {

    protected final Settings settings;
    protected final PlaylistSettings playlistSettings;

    public LakkaGamelist2ShellScript(
            final Settings settings,
            final PlaylistSettings playlistSettings,
            final String outputFileName) {
        super(settings.getOutputFolder(), outputFileName);
        this.settings = settings;
        this.playlistSettings = playlistSettings;
    }
}
