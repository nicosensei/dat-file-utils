package com.gitlab.nicosensei.retrogaming.lakka.playlist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public final class Playlist {

    private static final String DEFAULT_VERSION = "1.2";
    private static final String EMPTY = "";

    private String version = DEFAULT_VERSION;
    private String defaultCorePath = EMPTY;
    private String defaultCoreName = EMPTY;
    private int labelDisplayMode = 0;
    private int rightThumbnailMode = 0;
    private int leftThumbnailMode = 0;
    private List<Game> items = new ArrayList<>(500);

    public String getVersion() {
        return version;
    }

    public Playlist setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getDefaultCorePath() {
        return defaultCorePath;
    }

    public Playlist setDefaultCorePath(String defaultCorePath) {
        this.defaultCorePath = defaultCorePath;
        return this;
    }

    public String getDefaultCoreName() {
        return defaultCoreName;
    }

    public Playlist setDefaultCoreName(String defaultCoreName) {
        this.defaultCoreName = defaultCoreName;
        return this;
    }

    public int getLabelDisplayMode() {
        return labelDisplayMode;
    }

    public Playlist setLabelDisplayMode(int labelDisplayMode) {
        this.labelDisplayMode = labelDisplayMode;
        return this;
    }

    public int getRightThumbnailMode() {
        return rightThumbnailMode;
    }

    public Playlist setRightThumbnailMode(int rightThumbnailMode) {
        this.rightThumbnailMode = rightThumbnailMode;
        return this;
    }

    public int getLeftThumbnailMode() {
        return leftThumbnailMode;
    }

    public Playlist setLeftThumbnailMode(int leftThumbnailMode) {
        this.leftThumbnailMode = leftThumbnailMode;
        return this;
    }

    public List<Game> getItems() {
        return items;
    }

    public Playlist setItems(List<Game> items) {
        this.items = items;
        return this;
    }

    public Playlist addItem(Game ... item) {
        this.items.addAll(Arrays.asList(item));
        return this;
    }
}
