package com.gitlab.nicosensei.retrogaming.lakka.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGame;
import com.gitlab.nicosensei.retrogaming.lakka.RetroarchCore;
import com.gitlab.nicosensei.retrogaming.lakka.playlist.Game;
import com.gitlab.nicosensei.retrogaming.lakka.playlist.Playlist;
import com.gitlab.nicosensei.retrogaming.lakka.settings.PlaylistSettings;
import com.gitlab.nicosensei.retrogaming.lakka.settings.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public final class Gamelist2Playlist extends LakkaGamelist2AggregateFile<Playlist> {

    private static final Logger LOG = LoggerFactory.getLogger(Gamelist2Playlist.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    private static final String PLAYLIST_EXT = ".lpl";
    private static final String ZIP = "zip";
    private static final String HASHMARK = "#";
    private static final String DOT = ".";
    private static final String LAKKA_STORAGE_ROOT = "/storage";


    public Gamelist2Playlist(
            final Settings settings,
            final PlaylistSettings playlistSettings) {
        super(settings, playlistSettings);
    }

    @Override
    protected Playlist initializeResult() {
        return new Playlist();
    }

    @Override
    protected void updateResult(Playlist playlist, SparseGame src) throws IOException {
        final String playlistName = this.playlistSettings.getName() + PLAYLIST_EXT;
        final Game game = new Game().setDbName(playlistName);

        String romPath = SparseGame.dotConjoin(src.getPath());
        final String romName = src.getPath().getLeft();
        final String zipPath = playlistSettings.getRomFolder() + File.separator +  romPath;

        if (!playlistSettings.getPlatform().isArcade() && ZIP.equals(src.getPath().getRight())) {
            romPath = romPath + HASHMARK + getZippedRomPath(romName, zipPath);
        }

        game.setPath(zipPath.replace(settings.getLakkaDiskMountPoint(), LAKKA_STORAGE_ROOT));

        final RetroarchCore core = playlistSettings.getCore();
        if (core != null) {
            game.setCorePath(core.getPath());
            game.setCoreName(core.getName());
        }

        final String crc = src.getHash();
        if (crc != null) {
            game.setCrc32(crc);
        }

        game.setLabel(this.playlistSettings.isUseGamelistName() ? src.getPath().getLeft() : src.getName());

        playlist.addItem(game);
        LOG.info("Added {} ({})", game.getLabel(), game.getPath());
    }

    @Override
    protected String serialize(Playlist playlist) throws IOException {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(playlist);
    }

    @Override
    protected String outputFileName() {
        return playlistSettings.getName() + PLAYLIST_EXT;
    }

    private final String getZippedRomPath(final String romName, final String zipPath) throws IOException {
        final ZipFile zip = new ZipFile(zipPath, StandardCharsets.UTF_8);
        final Enumeration<? extends ZipEntry> entries = zip.entries();
        String rom = null;

        while (entries.hasMoreElements() && rom == null) {
            final String e = entries.nextElement().getName();
            if (playlistSettings.getExtensions()
                    .stream().map(ext -> e.endsWith(DOT + ext) || e.endsWith(DOT + ext.toUpperCase()))
                    .filter(b -> b)
                    .findFirst().isPresent()) {
                rom = e;
            }
        }

        if (rom == null) {
            throw new IOException(String.format("Could not find rom name '%s' in '%s'", romName, zipPath));
        }
        return rom;
    }

}
