package com.gitlab.nicosensei.retrogaming.lakka.processor;

import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGame;
import com.gitlab.nicosensei.retrogaming.lakka.Platform;
import com.gitlab.nicosensei.retrogaming.lakka.settings.PlaylistSettings;
import com.gitlab.nicosensei.retrogaming.lakka.settings.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;

public final class AssetsCopyScript extends LakkaGamelist2ShellScript {

    private static final Logger LOG = LoggerFactory.getLogger(AssetsCopyScript.class);

    private static final String LAKKA_ASSETS_OVERRIDES_ROOT = "{0}/assets/xmb/{1}/png/";
    private static final String CP_SYSTEM_ICON = "cp -vf \"{0}/assets/xmb/{1}/png/{2}.png\" \"{0}/assets/xmb/{1}/png/{3}.png\"";
    private static final String CP_CONTENT_ICON = "cp -vf \"{0}/assets/xmb/{1}/png/{2}-content.png\" \"{0}/assets/xmb/{1}/png/{3}-content.png\"";
    private static final String CP_PLAYLIST = "cp -vf \"{0}/{1}.lpl\" \"{2}/playlists/{1}.lpl\"";
    private static final String NEWLINE = System.lineSeparator();
    private static final String FMT_SCRIPT_NAME = "assets_%s.sh";

    public AssetsCopyScript(
            final Settings settings,
            final PlaylistSettings playlistSettings) {
        super(settings, playlistSettings, String.format(FMT_SCRIPT_NAME, playlistSettings.getName()));
    }

    @Override
    protected void writeHeader(PrintWriter pw) {
        super.writeHeader(pw);
        final String playlistName = playlistSettings.getName();
        final String lakkaDiskMountPoint = settings.getLakkaDiskMountPoint();
        final Platform platform = playlistSettings.getPlatform();
        pw.println("mkdir -p " + MessageFormat.format(LAKKA_ASSETS_OVERRIDES_ROOT, lakkaDiskMountPoint, settings.getXmbTheme()));
        pw.println(MessageFormat.format(CP_SYSTEM_ICON, lakkaDiskMountPoint, settings.getXmbTheme(), platform.getIcon(), playlistName));
        pw.println(MessageFormat.format(CP_CONTENT_ICON, lakkaDiskMountPoint, settings.getXmbTheme(), platform.getIcon(), playlistName));
        pw.println(MessageFormat.format(CP_PLAYLIST, settings.getOutputFolder(), playlistName, lakkaDiskMountPoint));
    }

    @Override
    protected void updateResult(PrintWriter result, SparseGame game) { }

}
