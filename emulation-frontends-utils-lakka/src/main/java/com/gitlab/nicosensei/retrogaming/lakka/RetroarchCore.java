package com.gitlab.nicosensei.retrogaming.lakka;

public enum RetroarchCore {

    fbneo("Arcade (FinalBurn Neo)"),
    fbalpha2012("Arcade (FinalBurn Alpha 2012)"),
    gearsystem("Sega - MS/GG/SG-1000 (Gearsystem)"),
    gambatte("Nintendo - Game Boy / Color (Gambatte)"),
    gpsp("Nintendo - Game Boy Advance (gpSP)"),
    mame2003_plus("Arcade (MAME 2000)"),
    mame2000("Arcade (MAME 2003-Plus)"),
    picodrive("Sega - MS/MD/CD/32X (PicoDrive)"),
    fceumm("Nintendo - NES / Famicom (FCEUmm)"),
    mednafen_pce_fast("NEC - PC Engine / CD (Beetle PCE FAST)"),
    pcsx_rearmed("Sony - PlayStation (PCSX ReARMed)"),
    snes9x2002("Nintendo - SNES / Famicom (Snes9x 2002)"),
    snes9x2010("Nintendo - SNES / Famicom (Snes9x 2010)");

    private static final String FMT_CORE_PATH = "/tmp/cores/%s_libretro.so";

    private final String path;
    private final String name;

    private RetroarchCore(final String name) {
        this.path = String.format(FMT_CORE_PATH, name());
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }
}
