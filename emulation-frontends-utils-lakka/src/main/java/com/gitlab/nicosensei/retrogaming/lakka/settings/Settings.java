package com.gitlab.nicosensei.retrogaming.lakka.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gitlab.nicosensei.retrogaming.lakka.Platform;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public final class Settings {

    private String lakkaDiskMountPoint;

    private String xmbTheme;

    private String outputFolder;

    private List<PlaylistSettings> playlists = new ArrayList<>(Platform.values().length);

    public String getLakkaDiskMountPoint() {
        return lakkaDiskMountPoint;
    }

    public Settings setLakkaDiskMountPoint(String lakkaDiskMountPoint) {
        this.lakkaDiskMountPoint = lakkaDiskMountPoint;
        return this;
    }

    public String getXmbTheme() {
        return xmbTheme;
    }

    public Settings setXmbTheme(String xmbTheme) {
        this.xmbTheme = xmbTheme;
        return this;
    }

    public List<PlaylistSettings> getPlaylists() {
        return playlists;
    }

    public Settings setPlaylists(List<PlaylistSettings> playlists) {
        this.playlists.addAll(playlists);
        return this;
    }

    public Settings addPlaylist(final PlaylistSettings playlist) {
        this.playlists.add(playlist);
        return this;
    }

    public String getOutputFolder() {
        return outputFolder;
    }

    public Settings setOutputFolder(String outputFolder) {
        this.outputFolder = outputFolder;
        return this;
    }

}
