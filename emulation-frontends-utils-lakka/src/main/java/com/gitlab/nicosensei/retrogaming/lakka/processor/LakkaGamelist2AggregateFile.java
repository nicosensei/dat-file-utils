package com.gitlab.nicosensei.retrogaming.lakka.processor;

import com.gitlab.nicosensei.retrogaming.lakka.settings.PlaylistSettings;
import com.gitlab.nicosensei.retrogaming.lakka.settings.Settings;
import com.gitlab.nicosensei.retrogaming.processor.Gamelist2AggregateFile;
import com.gitlab.nicosensei.retrogaming.processor.GamelistProcessor;

public abstract class LakkaGamelist2AggregateFile<T> extends Gamelist2AggregateFile<T> {

    protected final Settings settings;
    protected final PlaylistSettings playlistSettings;

    public LakkaGamelist2AggregateFile(final Settings settings, final PlaylistSettings playlistSettings) {
        this.settings = settings;
        this.playlistSettings = playlistSettings;
    }

    protected String outputFolder() {
        return settings.getOutputFolder();
    }

}
