package com.gitlab.nicosensei.retrogaming.lakka.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gitlab.nicosensei.retrogaming.lakka.Platform;
import com.gitlab.nicosensei.retrogaming.lakka.RetroArchThumbnail;
import com.gitlab.nicosensei.retrogaming.lakka.RetroarchCore;

import java.util.*;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public final class PlaylistSettings {

    private static final String DEFAULT_GAMELIST = "gamelist.xml";

    private Platform platform;
    private String romFolder;
    private String gamelist = DEFAULT_GAMELIST;
    private String name;
    private Set<String> extensions = new LinkedHashSet<>(5);
    private boolean useGamelistName = true;
    private RetroarchCore core;

    private final Map<RetroArchThumbnail, String> previews =
            new LinkedHashMap<>(RetroArchThumbnail.values().length);

    public Platform getPlatform() {
        return platform;
    }

    public PlaylistSettings setPlatform(Platform platform) {
        this.platform = platform;
        return this;
    }

    public String getRomFolder() {
        return romFolder;
    }

    public PlaylistSettings setRomFolder(String romFolder) {
        this.romFolder = romFolder;
        return this;
    }

    public String getName() {
        return name;
    }

    public PlaylistSettings setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isUseGamelistName() {
        return useGamelistName;
    }

    public PlaylistSettings setUseGamelistName(boolean useGamelistName) {
        this.useGamelistName = useGamelistName;
        return this;
    }

    public String getGamelist() {
        return gamelist;
    }

    public PlaylistSettings setGamelist(String gamelist) {
        this.gamelist = gamelist;
        return this;
    }

    public Set<String> getExtensions() {
        return extensions;
    }

    public PlaylistSettings setExtensions(String ... extensions) {
        this.extensions.addAll(Arrays.asList(extensions));
        return this;
    }

    public Map<RetroArchThumbnail, String> getPreviews() {
        return previews;
    }

    public RetroarchCore getCore() {
        return core;
    }

    public PlaylistSettings setCore(RetroarchCore core) {
        this.core = core;
        return this;
    }

}
