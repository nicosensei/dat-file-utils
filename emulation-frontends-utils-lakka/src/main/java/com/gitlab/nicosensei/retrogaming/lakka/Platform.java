package com.gitlab.nicosensei.retrogaming.lakka;

public enum Platform {
    fba("FBNeo - Arcade Games", true),
    fbneo("FBNeo - Arcade Games", true),
    mame2003("MAME 2003", true),
    neogeo("Neo Geo", true),
    gb("Nintendo - Game Boy"),
    gbc("Nintendo - Game Boy Color"),
    gba("Nintendo - Game Boy Advance"),
    gamegear("Sega - Game Gear"),
    mastersystem("Sega - Master System - Mark III"),
    megadrive("Sega - Mega Drive - Genesis"),
    segacd("Sega - Mega-CD - Sega CD"),
    nes("Nintendo - Nintendo Entertainment System"),
    snes("Nintendo - Super Nintendo Entertainment System"),
    n64("Nintendo - Nintendo 64"),
    pcengine("NEC - PC Engine - TurboGrafx 16"),
    psx("Sony - PlayStation"),
    nds("Nintendo - Nintendo DS"),
    psp("Sony - PlayStation Portable"),
    dreamcast("Sega - Dreamcast");

    private final String icon;

    private final boolean arcade;

    Platform(final String icon) {
        this(icon, false);
    }

    Platform(final String icon, boolean arcade) {
        this.icon = icon;
        this.arcade = arcade;
    }

    public String getIcon() {
        return icon;
    }

    public boolean isArcade() {
        return arcade;
    }

}
