# lakka-customization-utils

Utilities and resources for customizing Lakka:

- generating custom playlists from EmulationStation gamelists
- moving media scraped by Skraper.NET to Lakka thumbnails paths

And also graphics resources to have custom theming and dynamic wallpapers. 

_Credits_:

The XMB icons and dynamic wallpapers reuse existing graphic resources, this section gives credit to the original authors:

* Pixel theme for EmulationStation (https://github.com/ehettervik/es-theme-pixel.git)
* Minijawn theme for EmulationStation (https://github.com/pacdude/es-theme-minijawn)
* Yoshi-Kun icons (Tatsuya79, see https://forums.libretro.com/t/yoshi-kun-icons-theme/8371)

Notes
-----

* Resizing 8-bit indexed colors PNGs can be done with: `for f in *.png; do mogrify -resize 32x -colors 250 "$f"; done`
* _steamgrid_ Skraper images are `jpg`, must be converted to `png` using something like `find . -name "steamgrid" | while read d; do mogrify -format png "$d"/*.jpg; done` 
