package com.gitlab.nicosensei.retrogaming.emulationstation;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public final class SparseGame {

    private Pair<String, String> path;
    private String name;
    private String hash;
    private Pair<String, String> image;
    private Pair<String, String> thumbnail;

    public Pair<String, String> getPath() {
        return path;
    }

    public SparseGame setPath(Pair<String, String> path) {
        this.path = path;
        return this;
    }

    public Pair<String, String> getImage() {
        return image;
    }

    public SparseGame setImage(Pair<String, String> image) {
        this.image = image;
        return this;
    }

    public Pair<String, String> getThumbnail() {
        return thumbnail;
    }

    public SparseGame setThumbnail(Pair<String, String> thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public String getName() {
        return name;
    }

    public SparseGame setName(String name) {
        this.name = name;
        return this;
    }

    public String getHash() {
        return hash;
    }

    public SparseGame setHash(String hash) {
        this.hash = hash;
        return this;
    }

    public static String dotConjoin(final Pair<String, String> p) {
        return Arrays.asList(p.getLeft(), p.getRight()).stream().collect(Collectors.joining("."));
    }
}
