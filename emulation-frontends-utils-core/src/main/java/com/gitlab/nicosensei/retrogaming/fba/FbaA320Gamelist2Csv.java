package com.gitlab.nicosensei.retrogaming.fba;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.stream.Collectors;

public final class FbaA320Gamelist2Csv {

    private static final Logger LOG = LoggerFactory.getLogger(FbaA320Gamelist2Csv.class);

    private static final String REGEX_PIPE = "\\|";
    private static final String PIPE = "|";
    private static final String NEOGEO = "Neo Geo";

    public static final void main(String ... args) throws IOException {
        final PrintWriter out = new PrintWriter(new FileWriter(args[1]));

        final BufferedReader in = new BufferedReader(new FileReader(args[0]));
        try {
            String line;
            while ((line = in.readLine()) != null) {
                final String[] parts = line.trim().split(REGEX_PIPE);
                if (parts.length == 9) {
                    final String rom = parts[1].trim();
                    final String status = parts[2].trim();
                    final String name = parts[3].trim();
                    final String hardware = parts[7].trim();
                    if (!status.isEmpty()) {
                        LOG.info("Exclude {} with status {}", rom, status);
                    } else if (NEOGEO.equals(hardware)) {
                        LOG.info("Exclude {} with hardware {}", rom, hardware);
                    } else {
                        out.println(Arrays.asList(rom, name).stream().collect(Collectors.joining(PIPE)));
                    }
                }
            }
        } finally {
            out.close();
            in.close();
        }
    }

}
