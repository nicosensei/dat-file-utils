package com.gitlab.nicosensei.retrogaming.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class Gamelist2File extends GamelistProcessor<PrintWriter> {

    private static final Logger LOG = LoggerFactory.getLogger(Gamelist2File.class);

    private final Path outputFilePath;

    public Gamelist2File(final String outputFolder, final String outputFileName) {
        this.outputFilePath = FileSystems.getDefault().getPath(outputFolder, outputFileName);
    }

    @Override
    protected PrintWriter initializeResult() throws IOException {
        Files.createDirectories(outputFilePath.getParent());
        if (Files.exists(outputFilePath)) {
            Files.delete(outputFilePath);
        }
        Files.createFile(outputFilePath);
        final PrintWriter pw =  new PrintWriter(new OutputStreamWriter(
                new FileOutputStream(outputFilePath.toFile()),
                StandardCharsets.UTF_8));

        writeHeader(pw);
        return pw;
    }

    protected abstract void writeHeader(final PrintWriter pw);

    protected abstract void writeFooter(final PrintWriter pw);

    protected final void finalizeResult(final PrintWriter result) throws IOException {
        writeFooter(result);
        result.flush();
        result.close();
        LOG.info("Wrote {}", outputFilePath.toString());
    }

}
