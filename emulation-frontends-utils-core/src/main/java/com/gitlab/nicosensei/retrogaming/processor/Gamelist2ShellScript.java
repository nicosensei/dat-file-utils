package com.gitlab.nicosensei.retrogaming.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;

public abstract class Gamelist2ShellScript extends Gamelist2File {

    private static final Logger LOG = LoggerFactory.getLogger(Gamelist2ShellScript.class);

    public Gamelist2ShellScript(final String outputFolder, final String outputFileName) {
        super(outputFolder, outputFileName);
    }

    protected void writeHeader(final PrintWriter pw) {
        pw.println("#!/bin/bash");
        pw.println();
    }

    protected void writeFooter(final PrintWriter pw) {

    }

}
