package com.gitlab.nicosensei.retrogaming.emulationstation;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class SparseGamelist extends ArrayList<SparseGame> {

    public static final String REGEX_FILE_NAME = "([\\s\\p{L}\\p{N}\\(\\)\\[\\]-|\\p{Punct}]+)";
    public static final String REGEX_FILE_PATH = "\\./((?:[\\p{L}\\p{N}-]+/)+[\\s\\w\\(\\)\\[\\]-|\\p{Punct}]+)";
    public static final String REGEX_FILE_EXT = "\\.((?:[a-zA-Z0-9]+))";

    public static final String SLASH = "/";
    public static final String DASH = "-";
    public static final String UNDERSCORE = "_";
    public static final String SPACE = " ";
    public static final String REGEX_MULTI_WHITESPACE = "\\s{2,}+";

    private enum Format {
        gameOpen("<game(?: id=\"\\d+\")?(?: source=\"[\\w\\.-]+\")?>"),
        gameClose("</game>"),
        gamePath("<path>\\./" + REGEX_FILE_NAME + REGEX_FILE_EXT + "</path>", 2),
        gameName("<name>" + REGEX_FILE_NAME + "</name>", 1),
        gameHash("<hash>([A-Za-z0-9]{8}?)</hash>", 1),
        gameImage("<image>" + REGEX_FILE_PATH + REGEX_FILE_EXT + "</image>", 2),
        gameThumbnail("<thumbnail>" + REGEX_FILE_PATH + REGEX_FILE_EXT + "</thumbnail>", 2);

        private final Pattern pattern;
        private final int groups;

        Format(final String pattern, final int groups) {
            this.pattern = Pattern.compile(pattern);
            this.groups = groups;
        }

        Format(final String pattern) {
            this(pattern, 0);
        }

        public static Pair<Format, Optional<List<String>>> match(final String s) {
            for (final Format f : values()) {
                final Matcher m = f.pattern.matcher(s);
                if (m.matches()) {
                    if (f.groups > 0) {
                        final ArrayList<String> captures = new ArrayList<>(f.groups);
                        for (int i = 1; i <= f.groups; i++) {
                            captures.add(StringEscapeUtils.unescapeXml(m.group(i)));
                        }
                        return Pair.of(f, Optional.of(captures));
                    }
                    return Pair.of(f, Optional.empty());
                }
            }
            return null;
        }
    }

    private final boolean useGamelistName;

    SparseGamelist(final boolean useGamelistName) {
        super(1000);
        this.useGamelistName = useGamelistName;
    }

    public boolean isUseGamelistName() {
        return useGamelistName;
    }

    public static final SparseGamelist load(final InputStream data, final boolean useGamelistName) throws IOException {

        final BufferedReader in = new BufferedReader(new InputStreamReader(data, StandardCharsets.UTF_8));

        final SparseGamelist gamelist = new SparseGamelist(useGamelistName);
        String line;
        SparseGame game = null;
        try {
            while ((line = in.readLine()) != null) {
                if (line.trim().isEmpty()) {
                    continue;
                }
                final Pair<Format, Optional<List<String>>> match = Format.match(line.trim());
                if (match != null) {
                    final Format format = match.getKey();

                    switch (format) {
                        case gameOpen:
                            game = new SparseGame();
                            break;
                        case gameClose:
                            gamelist.add(game);
                            game = null;
                            break;
                        case gamePath:
                            if (game != null) {
                                game.setPath(listToPair(match.getValue().get()));
                                if (!useGamelistName) {
                                    String name = game.getPath().getLeft();
                                    final int lastFileSepIdx = name.lastIndexOf(File.separator);
                                    if (lastFileSepIdx > 0) {
                                        name = name.substring(lastFileSepIdx + 1);
                                    }
                                    game.setName(name
                                            .replaceAll(UNDERSCORE, SPACE)
                                            .replaceAll(SLASH, DASH)
                                            .replaceAll(REGEX_MULTI_WHITESPACE, SPACE));
                                }
                            }
                            break;
                        case gameName:
                            if (game != null && useGamelistName) {
                                game.setName(match.getValue().get().get(0)
                                        .replaceAll(SLASH, DASH));
                            }
                            break;
                        case gameHash:
                            game.setHash(match.getValue().get().get(0));
                            break;
                        case gameImage:
                            if (game != null) {
                                game.setImage(listToPair(match.getValue().get()));
                            }
                            break;
                        case gameThumbnail:
                            game.setThumbnail(listToPair(match.getValue().get()));
                            break;
                    }
                }
            }
            return gamelist;
        } finally {
            in.close();
        }
    }

    private static Pair<String, String> listToPair(final List<String> l) {
        return Pair.of(l.get(0), l.get(1));
    }

}
