package com.gitlab.nicosensei.retrogaming.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

public abstract class Gamelist2AggregateFile<T> extends GamelistProcessor<T> {

    private static final Logger LOG = LoggerFactory.getLogger(Gamelist2AggregateFile.class);

    protected abstract String serialize(T result) throws IOException;

    protected abstract String outputFolder();
    protected abstract String outputFileName();

    @Override
    protected void finalizeResult(T result) throws IOException {
        final Path outputFolderPath =  FileSystems.getDefault().getPath(outputFolder());
        Files.createDirectories(outputFolderPath);

        final Path output = FileSystems.getDefault().getPath(outputFolderPath.toString(), outputFileName());
        Files.write(
                output,
                Arrays.asList(serialize(result)),
                StandardCharsets.UTF_8,
                Files.exists(output) ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE_NEW);
        LOG.info("Generated aggregate file {}", output.toString());
    }
}
