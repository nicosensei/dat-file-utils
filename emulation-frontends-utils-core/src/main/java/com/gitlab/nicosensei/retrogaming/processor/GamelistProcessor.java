package com.gitlab.nicosensei.retrogaming.processor;

import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGame;
import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGamelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;

public abstract class GamelistProcessor<T> {

    private static final Logger LOG = LoggerFactory.getLogger(GamelistProcessor.class);

    public final void process(final SparseGamelist ... gamelist) throws IOException {
        final T result = initializeResult();
        for (final SparseGamelist gl : gamelist) {
            for (final SparseGame g : gl) {
                updateResult(result, g);
            }
        }
        finalizeResult(result);
    }

    protected abstract T initializeResult() throws IOException;

    protected abstract void updateResult(T result, SparseGame game) throws IOException;

    protected abstract void finalizeResult(final T result) throws IOException;
}
