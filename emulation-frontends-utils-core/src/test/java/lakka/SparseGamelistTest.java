package lakka;

import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGamelist;
import com.gitlab.nicosensei.retrogaming.emulationstation.SparseGame;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class SparseGamelistTest {

    private static final Pair<String, String> MISSING = Pair.of("?", "?");
    private static final Collector<CharSequence, ?, String> NEWLINE_JOINER = Collectors.joining(System.lineSeparator());

    @Test
    public final void testMasterSystemGamelist() throws IOException {
        final SparseGamelist gl = SparseGamelist.load(
                SparseGamelist.class.getResourceAsStream("/emulationstation/mastersystem_gamelist.dat"), true);

        assertEquals(30, gl.size());
        assertEquals(
                IOUtils.readLines(
                        SparseGamelistTest.class.getResourceAsStream(
                                "/emulationstation/mastersystem_dump.txt"), StandardCharsets.UTF_8).stream()
                        .collect(NEWLINE_JOINER),
                gl.stream().map(g -> Arrays.asList(
                        SparseGame.dotConjoin(g.getPath()),
                        g.getName(),
                        g.getHash() != null ? g.getHash() : "?",
                        SparseGame.dotConjoin(g.getImage() != null ? g.getImage() : MISSING),
                        SparseGame.dotConjoin(g.getThumbnail() != null ? g.getThumbnail() : MISSING))
                        .stream().collect(NEWLINE_JOINER))
                        .collect(Collectors.toList())
                        .stream().collect(NEWLINE_JOINER));

    }

}
